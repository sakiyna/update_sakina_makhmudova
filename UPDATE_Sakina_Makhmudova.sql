-- Task 1:
UPDATE film
SET rental_duration = 21,
    rental_rate = 9.99 
WHERE title = 'Interstellar';

--Task 2, 3:
UPDATE customer
SET first_name = 'Sakina',
    last_name = 'Makhmudova',
    email = 'sakinamt24@gmail.com',
    address_id = (SELECT address_id FROM address WHERE address = '613 Korolev Drive'),
    create_date = CURRENT_DATE
WHERE customer_id IN (
    SELECT customer_id
    FROM (
        SELECT customer_id
        FROM rental
        GROUP BY customer_id
        HAVING COUNT(rental_id) >= 10
    ) AS subquery1
    INTERSECT
    SELECT customer_id
    FROM (
        SELECT customer_id
        FROM payment
        GROUP BY customer_id
        HAVING COUNT(payment_id) >= 10
    ) AS subquery2
);


